Logotipo Queris
===============

Este proyecto contiene las distintas ediciones del logotipo
[Queris](https://bitbucket.org/alfa30/queris), con el fin de
estandarizar el desarrollo basado en la imagen Queris.

Este repositorio también contiene ediciones del mismo formato del
logotipo, Mejoras y definiciones de la imagen.

Herramientas de desarrollo
--------------------------

Este proyecto esta construido en lenguaje vectorial SVG. Todos los
proyectos relacionados deben seguir usando SVG como base para la edición
del mismo.

Se escogió este formato de imagen basado en su facilidad de manipulación
y edición.

Arquitectura de nuevos diseños
==============================

Publicar una nueva edición
--------------------------

Para llevar a cabo la edición de un nuevo diseño basado en esta imagen
se debe tener claro, el nombre que se quiera dar a esta versión de
logotipo, para luego incluir cuya edición dentro del directorio
`editions\` del mismo directorio raíz. Por ejemplo para para una edición
llamada "Jasmin", se debe de crear el archivo `logotipo.svg` con la
edición del logotipo y un directorio llamado `Jasmin\` dentro del
directorio `editions\`.

Quedando de la siguiente manera:

    .
    └── editions
        └── Jasmin
            └── logotipo.svg

Firmar la nueva edición
-----------------------

Tras finalizar la edición del nuevo logotipo se debe de incluir la firma
del mismo en el documento SVG, mediante los meta-datos del documento,
identificando Nombre del proyecto, Fecha, Nombre del creador entre otros
datos e identificar la licencia [Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International
License](http://creativecommons.org/licenses/by-nc-sa/4.0/) o superior.

### Firmar documento usando Inkscape

Si utilizas editor Inkscape para llevar a cabo la edición se puede
acceder al panel de meta-datos accediendo desde la pestaña "Archivo \>
Metadatos del documento...".

### Firmar documento manualmente desde editor de texto

En caso de no poseer medios o programas para realizar la firma del
documento, se puede hacer mediante el uso de cualquier editor de texto,
para lo que debemos tomar nuestro documento SVG y abrirlo con algún
editor de texto convencional. En caso de windows ***'notepad'***, Linux
***'gedit'***, Mac ***'macuarium'*** terminal ***'vi'***.

#### Etiqueta Title

Teniendo abierto el documento SVG se debe incluir o editar la etiqueta
`title` del documento, cuya función es definir el titulo del documento y
es donde debe ir el nombre de la edición.

    <title></title>

Una ves editado quedara algo así:

    <title>Logotipo V2</title>

#### Etiqueta Metadata

Esta etiqueta contiene dentro un conjunto de propiedades que sirven para
firmar nuestro documento SVG.

    <metadata>
      <rdf:RDF>
        <cc:Work rdf:about="">
          <dc:format>image/svg+xml</dc:format>
          <dc:type
             rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
          <dc:title>Logotipo V2</dc:title>
          <dc:creator>
            <cc:Agent>
              <dc:title>Jonathan Delgado Zamorano</dc:title>
            </cc:Agent>
          </dc:creator>
          <dc:rights>
            <cc:Agent>
              <dc:title>Jonathan Delgado Zamorano y colaboradores</dc:title>
            </cc:Agent>
          </dc:rights>
          <dc:publisher>
            <cc:Agent>
              <dc:title>Jonathan Delgado Zamorano</dc:title>
            </cc:Agent>
          </dc:publisher>
          <dc:identifier>https://bitbucket.org/alfa30/logotipo-queris/src/master/Logotipo.svg</dc:identifier>
          <dc:source>https://bitbucket.org/alfa30/logotipo-queris/src/</dc:source>
          <dc:language>es-CL</dc:language>
          <dc:subject>
            <rdf:Bag>
              <rdf:li>Logotipo</rdf:li>
              <rdf:li>Queris</rdf:li>
              <rdf:li>Imagen.</rdf:li>
            </rdf:Bag>
          </dc:subject>
          <dc:coverage>Distribución para la comunidad de desarrollo</dc:coverage>
          <dc:description>Logotipo de Queris.me diseñada para el uso de las aplicaciones de queris.</dc:description>
          <dc:relation>https://bitbucket.org/alfa30/logotipo-queris/src/master/README.md</dc:relation>
          <dc:date>jue 10 jul 2014 21:06:37 CLT</dc:date>
          <cc:license
             rdf:resource="http://creativecommons.org/licenses/by-nc-sa/4.0/" />
        </cc:Work>
      </rdf:RDF>
    </metadata>

1)  dc:title
      ~ Al igual que la etiqueta `title` mencionada anteriormente este
        sirve para poder definir el nombre del documento. Ejemplo
        `<dc:title>Logotipo V2</dc:title>`.

2)  dc:creator
      ~ Esta etiqueta identifica al creado de la obra, para lo que se
        debe de identificar dentro de la sub etiqueta `dc:title` que se
        encuentra dentro de `cc:Agent`. Ejemplo
        `<dc:creator><cc:Agent><dc:title>Jonathan Delgado Zamorano</dc:title></cc:Agent></dc:creator>`

3)  dc:rights
      ~ Esta etiqueta define los derechos de autor de la obra definiendo
        en cual debe incluir a quien se le atribuye la obra y de igual
        forma que a la etiqueta `dc:creator` se debe definir dentro de
        la etiqueta `dc:title`. Ejemplo
        `<dc:rights><cc:Agent><dc:title>Jonathan Delgado Zamorano y colaboradores</dc:title></cc:Agent></dc:rights>`

4)  dc:publisher
      ~ Esta etiquete define quien ha publicado la obra, por lo genera
        es el mismo que la ha creado pero puede ser un tercero. Ejemplo
        `<dc:publisher><cc:Agent><dc:title>Jonathan Delgado Zamorano</dc:title></cc:Agent></dc:publisher>`

5)  dc:coverage
      ~ Esta etiqueta define una breve descripción para el publico
        objetivo. Ejemplo
        `<dc:coverage>Distribución para la comunidad de desarrollo</dc:coverage>`

6)  dc:description
      ~ Esta etiquete define una descripción al documento. Ejemplo
        `<dc:description>Logotipo de Queris.me diseñada para el uso de las aplicaciones de queris.</dc:description>`

7)  dc:date
      ~ Esta etiqueta define la fecha en la que se finaliza la edición.
        Ejemplo `<dc:date>jue 10 jul 2014 21:06:37 CLT</dc:date>`

8)  cc:license
      ~ Esta etiqueta define la licencia sujeta a la edición lo cual se
        debe definir con la licencia [Creative Commons
        Attribution-NonCommercial-ShareAlike 4.0 International
        License](http://creativecommons.org/licenses/by-nc-sa/4.0/) o
        superior, respetando a la licencia original del documento y
        especificar la URI en el atributo de la etiqueta `rdf:resource`
        . Ejemplo
        `<cc:license rdf:resource="http://creativecommons.org/licenses/by-nc-sa/4.0/" />`

Licencia de la obra
===================

ver [LICENSE.txt](./LICENSE.txt)

![Licencia Creative
Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Logotipo Queris por [Jonathan Delgado Zamorano](http://jonad.in/) se
distribuye bajo una [Licencia Creative Commons
Atribución-NoComercial-CompartirIgual 4.0
Internacional](http://creativecommons.org/licenses/by-nc-sa/4.0/).

Basada en una obra en
[https://bitbucket.org/alfa30/logotipo-queris](https://bitbucket.org/alfa30/logotipo-queris).

Esta obra está licenciada bajo la Licencia Creative Commons
Atribución-NoComercial-CompartirIgual 4.0 Internacional. Para ver una
copia de esta licencia, visita
<http://creativecommons.org/licenses/by-nc-sa/4.0/>.
